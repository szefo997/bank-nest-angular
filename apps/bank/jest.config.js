module.exports = {
  name: 'bank',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/bank',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
