import { getGreeting } from '../support/app.po';

describe('bank', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to bank!');
  });
});
